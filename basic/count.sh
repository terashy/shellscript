#!/usr/bin/env bash

i=2
n=20

while ((i < n)); do
    i=$((i + 1))
    echo "$i"
done
